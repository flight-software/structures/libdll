cmake_minimum_required(VERSION 3.7)

if(TARGET dll)
    return()
endif()

project(libdll)

include(ExternalProject)

set(CMAKE_C_STANDARD 99)
set(CMAKE_SYSTEM_NAME Linux)

set(DEBUG ON CACHE BOOL "Enable DEBUG build")
set(STRIP OFF CACHE BOOL "Strip binary")
set(CROSS_COMPILE OFF CACHE BOOL "Build for flight")

function(set_compile_options target)
    target_compile_options(${target} PRIVATE
        -std=c99
        -Werror
        -Wfatal-errors
        -pedantic-errors
        -Wextra
        -Wall
        -Winit-self
        -Wmissing-include-dirs
        -Wswitch-default
        -Wswitch
        -Wfloat-equal
        -Wshadow
        -Wunsafe-loop-optimizations
        -Wcast-qual
        -Wconversion
        -Wlogical-op
        -Waggregate-return
        -Wstrict-prototypes
        -Wmissing-prototypes
        -Wmissing-declarations
        -Wnormalized=nfc
        -Wpacked
        -Wpadded
        -Wredundant-decls
        -Wnested-externs
        -Wunreachable-code
        -Wno-format
        -Wno-error=unused-function
        -Wno-error=sign-conversion
        -Wno-error=padded
        -Wno-error=unsafe-loop-optimizations
        -fno-strict-aliasing
    )
endfunction()

function(set_debug_options target debug cross)
    if(${debug})
        target_compile_options(${target} PRIVATE
            -Og
            -g
            -DDEBUG
        )
    else()
        target_compile_options(${target} PRIVATE
            -O3
            -Winline
            -DNDEBUG
        )
    endif()
endfunction()

set(FS_LIB_DLL_NAME dll)
set(FS_LIB_DLL_FILE lib${FS_LIB_DLL_NAME}.so)

set(FS_LIB_DLL_SRC ${PROJECT_SOURCE_DIR}/lib/libdll.c)
set(FS_LIB_DLL_HEADER ${PROJECT_SOURCE_DIR}/lib/libdll.h)

add_library(${FS_LIB_DLL_NAME} SHARED ${FS_LIB_DLL_SRC} ${FS_LIB_DLL_HEADER})

set_compile_options(${FS_LIB_DLL_NAME})

set_debug_options(${FS_LIB_DLL_NAME} ${DEBUG} ${CROSS_COMPILE})

target_include_directories(${FS_LIB_DLL_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/lib)

add_subdirectory(${PROJECT_SOURCE_DIR}/libdebug)

add_dependencies(${FS_LIB_DLL_NAME} debug_lib)

target_link_libraries(${FS_LIB_DLL_NAME} PRIVATE debug_lib)

if(${CROSS_COMPILE})
    set(CMAKE_SYSTEM_PROCESSOR arm)
    set(CROSS_COMPILER_PREFIX "/usr")
    
    set(CROSS_COMPILER_BINDIR ${CROSS_COMPILER_PREFIX}/bin)
    set(CROSS_COMPILER_INCLUDE ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf/usr/include)
    
    set(CMAKE_AR ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ar CACHE FILEPATH "Archiver")
    set(CMAKE_CXX_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-g++)
    set(CMAKE_C_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-gcc)
    set(CMAKE_LINKER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ld)
    set(CMAKE_NM ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-nm)
    set(CMAKE_OBJCOPY ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objcopy)
    set(CMAKE_OBJDUMP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objdump)
    set(CMAKE_RANLIB ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ranlib CACHE FILEPATH "Runlib")
    set(CMAKE_STRIP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-strip)

    set(CMAKE_FIND_ROOT_PATH ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf)

    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
    
    if(${STRIP})
        add_custom_command(TARGET ${FS_LIB_DLL_NAME}
            POST_BUILD
            COMMAND ${CMAKE_STRIP} --strip-all ${FS_LIB_DLL_FILE}
        )
    endif()
else()
    if(${STRIP})
        add_custom_command(TARGET ${FS_LIB_DLL_NAME}
            POST_BUILD
            COMMAND strip --strip-all ${FS_LIB_DLL_FILE}
        )
    endif()
endif()
