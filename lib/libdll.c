#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "libdebug.h"
#include "libdll.h"
/** @file */

/**
 * \brief Write contents of current disk to file
 *
 * Format on disk is sizeof(size_t) bytes for the length, then the payload
 *
 * If you're structure has pointers inside it, it shouldn't be,
 * flushed to disk
 *
 * Size is little endian
 */
static int dll_disk_write(const dll_fs* list)
{
    if(!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    if(!list->path_disk) return EXIT_SUCCESS;

    unlink(list->path_disk);
    FILE* file = fopen(list->path_disk, "w");
    if(!file) {
        P_ERR("file is null, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    dll_elem_fs* elem = list->head;
    while(elem) {
        if(fwrite(&(elem->size), 1, sizeof(elem->size), file) != sizeof(elem->size)) {
            P_ERR("Failed to write %zu bytes", sizeof(elem->size));
            fclose(file);
            return EXIT_FAILURE;
        }
        if(fwrite(elem->data, 1, elem->size, file) != elem->size) {
            P_ERR("Failed to write %zu bytes", elem->size);
            fclose(file);
            return EXIT_FAILURE;
        }
        elem = elem->next;
    }
    fclose(file);

    return EXIT_SUCCESS;
}

/**
 * \brief Read contents from disk to linked list
 */
static int dll_disk_read(dll_fs* list)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }
    
    if(!list->path_disk) return EXIT_SUCCESS;

    FILE* file = fopen(list->path_disk, "r");
    if (!file) {
        P_ERR("file is null, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    
    while(true) {
        size_t size = 0;
        if(fread(&size, 1, sizeof(size), file) != sizeof(size)) {
            break;
        }
        
        char* tmp_buf = malloc(size);

        if(fread(tmp_buf, 1, size, file) != size) {
            P_ERR("Failed to read %zu bytes", size);
            return EXIT_FAILURE;
        } else if(dll_push_tail(list, tmp_buf, size, DLL_NOWRITE) == EXIT_FAILURE) {
            fclose(file);
            return EXIT_FAILURE;
        }
    }
    fclose(file);
    return EXIT_SUCCESS;
}

/**
 * \brief Initialize a doubly linked list
 *
 * Pretty standard doubly linked list, except sizes can get a bit hairy
 *
 * - path_disk is a file backing of this structure, so we can be persistent
 *   across loads, can be NULL
 */

int dll_init(dll_fs* list, const char* path_disk)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    memset(list, 0, sizeof(dll_fs));

    if(path_disk) {
        FILE* fp;
        if((fp = fopen(path_disk, "a+")) == NULL) {
            P_ERR("Failed to open file %s with a+ mode, errno: %d (%s)", path_disk, errno, strerror(errno));
            return EXIT_FAILURE;
        }
        fclose(fp);
        list->path_disk = strdup(path_disk);
        if (dll_disk_read(list) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Destroy a doubly linked list
 *
 * list is not destroyed itself, but the pointer is passed so we ensure
 * the data it points to is up to date
 */

int dll_close(dll_fs* list)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    dll_clear(list);

    free(list->path_disk);

    return EXIT_SUCCESS;
}

/**
 * \brief Clear the list
 *
 * Clears all entries, keeps reserve filled 
 */
int dll_clear(dll_fs* list)
{
    if(!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    dll_elem_fs* cur;
    while((cur = dll_pull_head(list))) {
        free(cur);
    }

    return EXIT_SUCCESS;
}


/**
 * \brief Push some data to the head of the linked list
 */
int dll_push_head(dll_fs* list, void* data, size_t len, uint8_t flags)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if (!data) {
        P_ERR_STR("data is null");
        return EXIT_FAILURE;
    } else if((list->head != NULL) ^ (list->tail != NULL)) {
        P_ERR("Head and tail are not consistent, head %p, tail %p", list->head, list->tail);
        return EXIT_FAILURE;
    }
    
    dll_elem_fs* elem = malloc(sizeof(dll_elem_fs));

    if (!elem) {
        P_ERR("malloc failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    
    elem->next = NULL;
    elem->prev = NULL;
    elem->size = len;
    elem->data = data;

    if(!list->head) {
        list->head = elem;
        list->tail = elem;
    } else {
        list->head->prev = elem;
        elem->next = list->head;
        list->head = elem;
    }

    list->cur_size++;

    if(!(flags & DLL_NOWRITE)) {
        if(dll_disk_write(list) == EXIT_FAILURE) {
            P_ERR_STR("Error writing to disk");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Push some data to the tail of the linked list
 */
int dll_push_tail(dll_fs* list, void* data, size_t len, uint8_t flags)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if (!data) {
        P_ERR_STR("data is null");
        return EXIT_FAILURE;
    } else if((list->head != NULL) ^ (list->tail != NULL)) {
        P_ERR("Head and tail are not consistent, head %p, tail %p", list->head, list->tail);
        return EXIT_FAILURE;
    }
    
    dll_elem_fs* elem = malloc(sizeof(dll_elem_fs));

    if (!elem) {
        P_ERR("malloc failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    elem->next = NULL;
    elem->prev = NULL;
    elem->size = len;
    elem->data = data;

    if(!list->head) {
        list->head = elem;
        list->tail = elem;
    }else{
        list->tail->next = elem;
        elem->prev = list->tail;
        list->tail = elem;
    }

    list->cur_size++;

    if(!(flags & DLL_NOWRITE)) {
        if(dll_disk_write(list) == EXIT_FAILURE) {
            P_ERR_STR("Error writing to disk");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Pull node from head of list
 */

void* dll_pull_head(dll_fs* list)
{
    if(!list || !list->head) {
        return NULL;
    }
    
    void* retval = list->head->data;
    void* old_head = list->head;
    
    list->head = list->head->next;
    if(!list->head) {
        list->tail = NULL;
    } else {
        list->head->prev = NULL;
    }

    list->cur_size--;

    free(old_head);

    dll_disk_write(list);

    return retval;
}
/**
 * \brief Pull node from tail of list
 */
void* dll_pull_tail(dll_fs* list)
{
    if(!list || !list->tail) {
        return NULL;
    }

    void* retval = list->tail->data;
    void* old_tail = list->tail;

    list->tail = list->tail->prev;
    if(!list->tail) {
        list->head = NULL;
    } else {
        list->tail->next = NULL;
    }
    
    list->cur_size--;
    
    free(old_tail);
    
    old_tail = NULL;

    dll_disk_write(list);

    return retval;
}

int dll_pull_ptr(dll_fs* list, void* ptr)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if (!ptr) {
        P_ERR_STR("cannot remove non-existent pointer");
        return EXIT_FAILURE;
    }

    if (list->head == NULL) {
        return EXIT_FAILURE;
    } else if (ptr == list->head->data) {
        dll_pull_head(list);
        return EXIT_SUCCESS;
    } else if (ptr == list->tail->data) {
        dll_peek_tail(list);
        return EXIT_SUCCESS;
    }

    dll_elem_fs* cur = list->head->next;

    while(cur) {
        if(ptr == cur->data) {
            if(cur->next) {
                cur->next->prev = cur->prev;
            }
            if(cur->prev) {
                cur->prev->next = cur->next;
            }
            if(cur == list->head) {
                list->head = NULL;
            }
            if(cur == list->tail) {
                list->tail = NULL;
            }
            list->cur_size--;
            free(cur);

            if(dll_disk_write(list) == EXIT_FAILURE) {
                P_ERR_STR("writing to disk");
                return EXIT_FAILURE;
            }
            
            return EXIT_SUCCESS;
        }
        cur = cur->next;
    }

    return EXIT_FAILURE;
}

void* dll_peek_head(const dll_fs* list)
{
    if(list && list->head) {
        return list->head->data;
    }
    return NULL;
}

void* dll_peek_tail(const dll_fs* list)
{
    if(list && list->tail) {
        return list->tail->data;
    }
    return NULL;
}

/**
 * \brief Search linked list with a callback
 *
 * Pretty useful, returns the pointer to the payload
 * NOTE: this assumes providing NULL pointers as payloads is an error
 * Doesn't strictly have to be this way, but we use NULL when we can't
 * find a payload, so there's conflict there.
 */

void* dll_search(const dll_fs* list, uint8_t (*callback)(void*, void*), void* payload)
{
    if(!list || !callback) {
        return NULL;
    }
    
    dll_elem_fs* cur = list->head;

    while(cur) {
        if(!cur->data) {
             P_ERR_STR("We somehow managed to store a NULL payload");
             return NULL;
        }
        if(callback(cur->data, payload)) {
            return cur->data;
        }
        cur = cur->next;
    }
    return NULL;
}

