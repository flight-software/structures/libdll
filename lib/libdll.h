#ifndef LIB_DLL_H
#define LIB_DLL_H
/** @file */

#include <stdint.h>
#include <stdlib.h>

struct dll_elem_fs {
    struct dll_elem_fs* next;
    struct dll_elem_fs* prev;
    void* data;
    size_t size;
};

#define DLL_NOWRITE (1 << 1)

/**
 * \brief Doubly linked list
 *
 * Normal doubly linked list, except we have a list of nodes with pre-allocated
 * chunks of data, so we can append to this safely inside a signal handler (given
 * that we have a node in that list with the required criteria)
 *
 * NOTE: Whenever you use this as a queue, you should use push_head to enqueue
 * and poll_tail to dequeue (for the sake of continuity)
 */

struct dll_fs {
    struct dll_elem_fs* head;
    struct dll_elem_fs* tail;
    char* path_disk;
    size_t cur_size;
};

typedef struct dll_elem_fs dll_elem_fs;
typedef struct dll_fs dll_fs;

int dll_init(dll_fs* list, const char* path_disk);
int dll_close(dll_fs* list);
int dll_clear(dll_fs* list);

int dll_push_head(dll_fs* list, void* data, size_t len, uint8_t flags);
int dll_push_tail(dll_fs* list, void* data, size_t len, uint8_t flags);

void* dll_pull_head(dll_fs* list);
void* dll_pull_tail(dll_fs* list);

int dll_pull_ptr(dll_fs* list, void* ptr);

void* dll_peek_head(const dll_fs* list);
void* dll_peek_tail(const dll_fs* list);

void* dll_search(const dll_fs* list, uint8_t(*callback)(void*, void*), void* payload);

#define DLL_ITER(dll___, type___, elem___)                              \
    type___ *elem___ = NULL;                                            \
    dll_elem_fs *dll_elem ## elem___ = dll___.head;                                 \
    for(;dll_elem ## elem___ != NULL && (elem___ = dll_elem ## elem___ ->data) != NULL;dll_elem ## elem___ = dll_elem ## elem___ ->next)

#endif
